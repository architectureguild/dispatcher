﻿namespace Dispatcher
{
    public class NoWork : Work
    {
        public override IWorkResult ProcessWork()
        {
            //Do Nothing
            return new NoWorkResult();
        }
    }
}
