﻿using System.Threading;

namespace Dispatcher
{
    public class WriteQuoteResultToRedis : WorkResult
    {
        public override void PublishResult()
        {
            System.Console.WriteLine("{0} Writing response to redis...", currentNumber);
            Thread.Sleep(RNG.Next(1, 3) * 1000);
            System.Console.WriteLine("{0} Done", currentNumber);
        }
    }
}
