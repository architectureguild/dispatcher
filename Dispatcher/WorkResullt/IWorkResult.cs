﻿namespace Dispatcher
{
    public interface IWorkResult
    {
        void PublishResult();
    }
}
