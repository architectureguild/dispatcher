﻿using System;
using System.Threading.Tasks;

namespace Dispatcher
{
    public abstract class Dispatcher
    {
        public void Dispatch(IWorkListener listener)
        {
            var work = listener.ListenForWork();
            DoWork(work);
        }

        public void DispatchAsync(IWorkListener listener)
        {
            var work = listener.ListenForWork();

            Task.Run(() => DoWork(work));
        }

        protected abstract void DoWork(IWork work);
    }
}
