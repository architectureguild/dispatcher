﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Dispatcher
{
    public class RabbitMQListener : WorkGenerator
    {
        private readonly ConnectionFactory _factory;

        public RabbitMQListener()
        {
            _factory = new ConnectionFactory() { HostName = "localhost" };
        }

        public override IWork ListenForWork()
        {
            var work = String.Empty;
            using (var connection = _factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [<-->] Received {0}", message);
                    work = message;
                };
                channel.BasicConsume(queue: "hello",
                                     autoAck: true,
                                     consumer: consumer);
                Console.WriteLine("Waiting for work...");
                while(work.Equals(String.Empty))
                {
                    Thread.Sleep(100);
                }

                return new SerializeRequest();
            }
        }
    }
}
