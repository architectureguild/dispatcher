﻿using System.Threading;

namespace Dispatcher
{
    public class SerializeRequest : Work
    {
        public override IWorkResult ProcessWork()
        {
            System.Console.WriteLine("{0} Serializing request...", currentNumber);
            Thread.Sleep(RNG.Next(1, 3) * 1000);
            System.Console.WriteLine("{0} Done serializing request", currentNumber);

            return new WriteQuoteResultToRedis();
        }
    }
}
