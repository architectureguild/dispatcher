﻿using System;

namespace Dispatcher
{
    public abstract class WorkResult : IWorkResult
    {
        private static int counter = 0;
        protected int currentNumber;
        protected static readonly Random RNG = new Random(DateTime.Now.Millisecond);

        protected WorkResult()
        {
            currentNumber = counter++;
        }

        public abstract void PublishResult();
    }
}
