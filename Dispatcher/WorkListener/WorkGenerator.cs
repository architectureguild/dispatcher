﻿namespace Dispatcher
{
    public abstract class WorkGenerator : IWorkListener
    {
        private static int counter = 0;
        protected int currentNumber;

        protected WorkGenerator()
        {
            currentNumber = counter++;
        }

        public abstract IWork ListenForWork();
    }
}
