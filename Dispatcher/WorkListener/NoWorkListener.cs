﻿namespace Dispatcher
{
    public class NoWorkListener : WorkGenerator
    {
        public override IWork ListenForWork()
        {
            System.Console.ReadKey();
            //Do Nothing
            return new NoWork();
        }
    }
}
