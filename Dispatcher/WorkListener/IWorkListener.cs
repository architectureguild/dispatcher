﻿namespace Dispatcher
{
    public interface IWorkListener
    {
        IWork ListenForWork();
    }
}
